from .view import register_views
from flask import Flask


class App(Flask):
    def handle_exception(self, e):
        pass

    def apply_task(self, task, args=None, kwargs=None, *args_, **kwargs_):
        pass

    def send_notifications(self, notifications):
        pass


def create_app(config):
    app = Flask(__name__)
    register_views(app)
    return app


def hello_word():
    return 'hello word'