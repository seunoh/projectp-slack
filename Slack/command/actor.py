__author__ = 'seunoh'


class Actor():
    def __init__(self, command):
        self.command = command

    def execute(self):
        print(self.command)


class HelperActor(Actor):
    def __init__(self):
        super().__init__("HelperActor")


class StartActor(Actor):
    def __init__(self):
        super().__init__("StartActor")


class StopActor(Actor):
    def __init__(self):
        super().__init__("StopActor")


class StatusActor(Actor):
    def __init__(self):
        super().__init__("StatusActor")




