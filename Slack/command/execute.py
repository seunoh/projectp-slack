import collections
from ..command import COMMAND_MAP
__author__ = 'seunoh'

d = COMMAND_MAP


def execute(text):

    result_text = text;
    if text in d:
        actor = d.get(text, None)
        actor.execute()
    else:
        result_text = "command not found [%s] " % text
        result_text += " list ["

        od = collections.OrderedDict(sorted(d.items()))
        for key in od:
            result_text += "%s, " % key
        result_text = result_text[: -2]
        result_text += ']'
    return result_text








