from .index_view import main_blueprint

__author__ = 'seunoh'
__version__ = '0.1.0'


def register_views(app):
    app.register_blueprint(main_blueprint)