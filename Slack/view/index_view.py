from flask import Blueprint, request
from ..command.execute import *

main_blueprint = Blueprint('main', __name__)


@main_blueprint.route('/<text>')
def main(text):
    return parser_text(text)


def parser_text(text):
    if text[0:2] == '--':
        command = text[2:len(text)]
    else:
        command = text

    return '%s' % execute(command)

