import argparse
from Slack.app import create_app

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-l',
        '--local',
        dest='local',
        action='store_true',
        default=False,
        help='Run as local mode'
    )
    parser.add_argument(
        '-t',
        '--host',
        dest='host',
        default='0.0.0.0',
        help='Binding host address'
    )
    parser.add_argument(
        '-p',
        '--port',
        dest='port',
        default=8000,
        type=int,
        help='Port number'
    )

    args = parser.parse_args()
    local = args.local
    host = args.host
    port = args.port

    if local:
        #if not os.path.exists(local_config.SANDBOX):
        #    os.mkdir(local_config.SANDBOX)
        app = create_app('local_config')
        #if not os.path.exists(local_config.DB_PATH):
        #    models.rebuild_db()
        print('Running as local mode')
    else:
        app = create_app('config')
    app.run(host=host, port=port, threaded=True, debug=True)