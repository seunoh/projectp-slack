__author__ = 'seunoh'

import os
import sys
import setuptools
from setuptools import setup
#sys.path.insert(1, os.path.abspath('docs'))

requires = [
    # Flask related
    'Flask>=0.11',
    'Flask-Router',
    'Flask-Negotiation',
    #'Flask-Babel>=0.9',

    # SQL related
    #'SQLAlchemy',
    #'psycopg2',
    #'SQLAlchemy-ImageAttach>=0.8.0',

    # Form validation related
    #'Formencode',
    #'Formencode-Jinja2',

    # Image processing related
    #'wand>=0.3.0',

    # Push notification related
    #'apns-client>=0.1',
    #'gcm-client>=0.1',

    # Scalability
    #'Beaker',
    #'Celery>=3.0.0',

    # Documentation Requirements
    #'sphinxcontrib-httpdomain',
]

dependency_links = [
]

setup(name='ProjectP',
      version='0.1.0',
      author='Se-un, Oh', requires=['flask'],
      #author_email='zeros0317@gmail.com',
      #description='ProjectP Server',
      #long_description=__doc__,
      #packages=setuptools.find_packages(exclude='tests'),
      #include_package_data=True,
      #zip_safe=False,
      #platforms='any',
      #install_requires=requires,
      #dependency_links=dependency_links,
      #classifiers=[
      #    'Environment :: Web Environment',
      #]
      )
